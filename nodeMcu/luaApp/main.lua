  if file.exists("eus_params.lua") then
    -- Load network parameters (if exists)
    local p = dofile("eus_params.lua")
    local wifi_cfg={}
    wifi_cfg.ssid = p.wifi_ssid
    wifi_cfg.pwd = p.wifi_password
    wifi_cfg.save = true
    wifi.sta.config(wifi_cfg)
    wifi.sta.autoconnect(1)
    -- Try to connect
    wifi.sta.connect()

    disp:clearBuffer()    
    disp:drawRBox(0,0,128,16,4)
    disp:drawRFrame(0,16,128,48,4)
    disp:setDrawColor(0)
    local str = "Connecting"
    local x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 4, str)
    disp:setDrawColor(1)
    local cfg = wifi.sta.getconfig(true)
    str = "SSID: " .. cfg.ssid
    x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 24, str);
    disp:drawRFrame(24,45,80,10,4)        
    disp:sendBuffer()

    connCounter = 1;
    connTimer = tmr.create()
    connTimer:register(1300, 
      tmr.ALARM_AUTO, 
        function()      
          disp:drawRBox(24,45,(8*connCounter),10,4)
          disp:sendBuffer()     
          connCounter = connCounter + 1
          if(wifi.sta.status() == wifi.STA_GOTIP) then            
            connTimer:stop()
            connTimer:unregister()
            connTimer = nil
            disp:clearBuffer()
            disp:setDrawColor(1)
            disp:drawRBox(0,0,128,16,4)
            disp:drawRFrame(0,16,128,48,4)
            disp:setDrawColor(0)
            local str = "Connected"
            local x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 4, str)
            disp:setDrawColor(1)
            str = "Ip: " .. wifi.sta.getip()    
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 40, str);
            local cfg = wifi.sta.getconfig(true)
            str = "SSID: " .. cfg.ssid
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 24, str);
            disp:sendBuffer()           
            dofile("beginApp.lua")
          else if(connCounter == 11) then
            connTimer:stop();
            connTimer:unregister()
            connTimer = nil
            disp:clearBuffer()    
            disp:drawRBox(0,0,128,16,4)
            disp:drawRFrame(0,16,128,48,4)
            disp:setDrawColor(0)
            local str = "Setup Mode"
            local x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 4, str)
            disp:setDrawColor(1)
            local cfg = wifi.sta.getconfig(true)
            str = "Connect to net: "
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 19, str);
            local mac = wifi.sta.getmac() .. ":"
            local i = 0
            str = ""
            local w
            for w in mac:gmatch("(.-):") do 
              if i >=3 then
                 str = str .. string.upper(w)
              end
              i = i + 1
            end
            str = "SetupGadget_" .. str
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 29, str);
            str = "Browser to:"
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 40, str);
            str = "http://www.conf.com"
            x = disp:getStrWidth(str)
            disp:drawStr((128 - x)/2, 50, str);
            disp:sendBuffer()
            enduser_setup.start(
              function()                
                file.open("setupOk.xbm", "r")
                local icon = file.read()
                file.close()
                disp:clearBuffer()
                disp:drawXBM(0, 0, 128, 64, icon)
                disp:sendBuffer()                
                connTimer = tmr.create()
                connTimer:register(3000, 
                  tmr.ALARM_AUTO, 
                  function()  
                    enduser_setup.stop()
                    node.restart()
                  end)
                connTimer:start()
              end,
              function(err, str)            
                file.open("setupFail.xbm", "r")
                local icon = file.read()
                file.close();
                disp:clearBuffer()                
                disp:drawXBM(0, 0, 128, 64, icon)
                disp:sendBuffer()                
                connTimer = tmr.create()
                connTimer:register(3000, 
                  tmr.ALARM_AUTO, 
                  function()                      
                    node.restart()
                  end)
                connTimer:start()
             end)
           end
         end
       end)
    connTimer:start()
  else
    disp:clearBuffer()    
    disp:drawRBox(0,0,128,16,4)
    disp:drawRFrame(0,16,128,48,4)
    disp:setDrawColor(0)
    local str = "Setup Mode"
    local x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 4, str)
    disp:setDrawColor(1)
    local cfg = wifi.sta.getconfig(true)
    str = "Connect to net: "
    x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 19, str);
    local mac = wifi.sta.getmac() .. ":"
    i = 0
    str = ""
    for w in mac:gmatch("(.-):") do 
      if i >=3 then
        str = str .. string.upper(w)
      end
      i = i + 1
    end
    str = "SetupGadget_" .. str
    x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 29, str);
    str = "Browser to:"
    x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 40, str);
    str = "http://www.conf.com"
    x = disp:getStrWidth(str)
    disp:drawStr((128 - x)/2, 50, str);
    disp:sendBuffer()
enduser_setup.start(
              function()                
                file.open("setupOk.xbm", "r")
                local icon = file.read()
                file.close()
                disp:clearBuffer()
                disp:drawXBM(0, 0, 128, 64, icon)
                disp:sendBuffer()                
                connTimer = tmr.create()
                connTimer:register(3000, 
                  tmr.ALARM_AUTO, 
                  function()  
                    enduser_setup.stop()
                    node.restart()
                  end)
                connTimer:start()
              end,
              function(err, str)            
                file.open("setupFail.xbm", "r")
                local icon = file.read()
                file.close();
                disp:clearBuffer()                
                disp:drawXBM(0, 0, 128, 64, icon)
                disp:sendBuffer()                
                connTimer = tmr.create()
                connTimer:register(3000, 
                  tmr.ALARM_AUTO, 
                  function()                      
                    node.restart()
                  end)
                connTimer:start()
             end)
  end
