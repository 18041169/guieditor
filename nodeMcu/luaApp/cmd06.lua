--
--  Start pwm at ESP8266 pin
--
local retCmd = {}
local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(6)
  local str = "Start pwm out"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str);
  str = "Pin nro:" .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()
  pwm.start(sm["data"][1]);
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
