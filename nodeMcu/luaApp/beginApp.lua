-- STATE_HEADER 1
-- STATE_CMD    2
-- STATE_DATA   3
-- STATE_CRC    4
-- STATE_FOOTER 5

stateMachine = {};
stateMachine["cmd"] = 0;
-- Cuidado, es basado en 1 el índice
stateMachine["data"] = {0, 0, 0, 0, 0};
stateMachine["dataInx"] = 1;
stateMachine["crc"] = 0;
stateMachine["nextState"] = 1;


--
-- Definición de configuración para 
-- la interfaz de potencia.
--
cfgPinPower1 = {};
cfgPinPower1["pinIn"] = 1;
cfgPinPower1["pinOut"] = 2;
cfgPinPower1["enable"] = false;

cfgPinPower2 = {};
cfgPinPower2["pinIn"] = 1;
cfgPinPower2["pinOut"] = 2;
cfgPinPower2["enable"] = false;

cfgPinPower3 = {};
cfgPinPower3["pinIn"] = 1;
cfgPinPower3["pinOut"] = 2;
cfgPinPower3["enable"] = false;

cfgPinPower4 = {};
cfgPinPower4["pinIn"] = 1;
cfgPinPower4["pinOut"] = 2;
cfgPinPower4["enable"] = false;

cfgPinPower5 = {};
cfgPinPower5["pinIn"] = 1;
cfgPinPower5["pinOut"] = 2;
cfgPinPower5["enable"] = false;

cfgPinPower6 = {};
cfgPinPower6["pinIn"] = 1;
cfgPinPower6["pinOut"] = 2;
cfgPinPower6["enable"] = false;

powerCfg = {
  cfgPinPower1,
  cfgPinPower2,
  cfgPinPower3,
  cfgPinPower4,
  cfgPinPower5,
  cfgPinPower6
}


octaveSrv = net.createServer(net.TCP, 28800)

function stageHeader(sm, newVal, sck)  
  nroVal = string.byte(newVal, 1, 1);
  if(nroVal == 0xB0) then
    sm["crc"] = 0xB0;    
    sm["dataInx"] = 1;
    sm["nextState"] = 2; -- STATE_CMD
  else
    sm["nextState"] = 1; -- STATE_HEADER
  end
end

function stageCMD(sm, newVal, sck)
  nroVal = string.byte(newVal, 1, 1);
  if((nroVal >= 1) and (nroVal <= 128)) then
    sm["crc"] = sm["crc"] + nroVal;
    sm["cmd"] = nroVal;
    sm["nextState"] = 3; -- STATE_DATA
  else
    sm["nextState"] = 1; -- STATE_HEADER
  end
end

function stageDATA(sm, newVal, sck)
  nroVal = string.byte(newVal, 1, 1);
  sm["data"][sm["dataInx"]] = nroVal;
  sm["crc"] = sm["crc"] + nroVal;
  sm["dataInx"] = sm["dataInx"] + 1;
  if(sm["dataInx"] == 6) then
    sm["nextState"] = 4; -- STATE_CRC
  end
end

function stageCRC(sm, newVal, sck)
  nroVal = string.byte(newVal, 1, 1);
  sm["crc"] = sm["crc"] % 256;
  if(sm["crc"] == nroVal) then
    sm["nextState"] = 5; -- STATE_FOOTER
  else
    sm["nextState"] = 1; -- STATE_HEADER
  end
end

function stageFOOTER(sm, newVal, sck)
  nroVal = string.byte(newVal, 1, 1);    
  if(nroVal == 0x0B) then 
     doCmd = "cmd" 
     doCmd = doCmd .. string.upper(string.format("%02x", sm["cmd"]))
     doCmd = doCmd .. ".lua"
     if file.exists(doCmd) then
       dofile(doCmd).doCmd(sm, sck)
       collectgarbage()
     else
       print(doCmd .. ": not found")
     end 
  end
  sm["nextState"] = 1; -- STATE_HEADER;
end

smSteps = {stageHeader,
           stageCMD,
           stageDATA,
           stageCRC,
           stageFOOTER};

function receiver(sck, data)
  for i = 1, #data do
    state = stateMachine["nextState"];
    dataUnit = string.sub(data, i, i);    
    smSteps[state](stateMachine, dataUnit, sck);    
  end
end

if octaveSrv then
  octaveSrv:listen(1221, 
    function(conn)
      conn:on("receive", receiver)
      conn:on("disconnection", 
        function(sck, err)
          disp:clearBuffer()
          disp:setDrawColor(1)
          disp:drawRBox(0,0,128,16,4)
          disp:drawRFrame(0,16,128,48,4)
          disp:setDrawColor(0)
          local str = "Ready"
          local x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 4, str)
          disp:setDrawColor(1)
          str = "Ip: " .. wifi.sta.getip()    
          x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 40, str);
          local cfg = wifi.sta.getconfig(true)
          str = "SSID: " .. cfg.ssid
          x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 24, str);
          disp:sendBuffer()  
        end)
    end)
end
