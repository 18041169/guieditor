--
--  Set pin state (write pin)
--

local retCmd = {}
local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(3)
  local str = "Set pin value"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 22, str);

  str = "Pin: " .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 36, str)

  str = "State: " .. sm["data"][2]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 50, str)

  disp:sendBuffer()
  if (sm["data"][2] == 0) then
    gpio.write(sm["data"][1], 0)
  else
    gpio.write(sm["data"][1], 1)
  end
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
