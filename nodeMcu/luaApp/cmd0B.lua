--
--  Read temp using one wire with a DS18B20 sensor.
--  This command just return the actual value of sensor.
--
local retCmd = {}
local DS18B20 = 7 

local function getTemp(sck)
  ow.setup(DS18B20)
  tempValue = 0;
  tempStatus = false
  count = 0
  repeat
    count = count + 1
    addr = ow.reset_search(DS18B20)
    addr = ow.search(DS18B20)
    tmr.wdclr()
  until((addr ~= nil) or (count > 100))

  if (addr ~= nil) then
    crc = ow.crc8(string.sub(addr,1,7))
    if (crc == addr:byte(8)) then
      if ((addr:byte(1) == 0x10) or (addr:byte(1) == 0x28)) then
        ow.reset(DS18B20)
        ow.select(DS18B20, addr)
        ow.write(DS18B20, 0x44, 1)
        tmr.delay(1000000)
        present = ow.reset(DS18B20)
        ow.select(DS18B20, addr)
        ow.write(DS18B20,0xBE,1)
        data = nil
        data = string.char(ow.read(DS18B20))
        for i = 1, 8 do
          data = data .. string.char(ow.read(DS18B20))
        end
        crc = ow.crc8(string.sub(data,1,8))      
        if (crc == data:byte(9)) then
          t = (data:byte(1) + data:byte(2) * 256) * 625
          tempValue = t / 10000
          dofile("drawCmdBase.lua")(11)
          local str = "Get temp"
          local x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 22, str)
          str = "from DS18B20"
          x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 36, str)
          str = string.format("%0.2f", tempValue) .. " [C]"
          x = disp:getStrWidth(str)
          disp:drawStr((128 - x)/2, 50, str)
          disp:sendBuffer()
	  sck:send(string.char(data:byte(1)))
	  sck:send(string.char(data:byte(2)))
        end                   
        tmr.wdclr()
      end
    end
  end
end


local function doCmd(sm, sck)    
  disp:clearBuffer()
  dofile("drawCmdBase.lua")(11)
  local str = "Get temp"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 22, str)
  str = "from DS18B20"
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 36, str)
  str = "Acquiring..."
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 50, str)
  disp:sendBuffer()
  getTemp(sck)
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
