--
--  Get pin state (read value)
--

local retCmd = {}
local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(2)
  local str = "Return pin state"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 22, str);
  
  str = "Pin: " .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 36, str);
  
  ret = 0x00;
  if (gpio.read(sm["data"][1]) == 1) then
    ret = 0xFF;
    str = "State: H"
  else
    str = "State: L"
  end   
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 50, str);
  disp:sendBuffer()
  sck:send(string.char(ret))
end
retCmd.doCmd = doCmd
collectgarbage()
return retCmd
