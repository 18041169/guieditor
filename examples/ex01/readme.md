
# Example 01: basic app

Aspects of interest:
- An application is created from a project. The project name will be the name of the package for GNU Octave. Therefore it can only contain lowercase letters.
- The projects are based on dialog windows. In general the first step after creating a project is to add a dialog window.
- The dialog window (being the only one) will automatically become the main window and will appear when the application starts.
- Controls can be added to the window simply by clicking on the control of interest and then on the target dialog window. In this case they will have the default size. If desired, the control can be drawn in the window and in this case it will have the corresponding size.
- Controls can be aligned by selecting them and using the context menu (right mouse click on the selection).
- All controls have a default event, it will be executed when interacting with the control. In the case of a button, it corresponds to clicking on it.
- If you want to add arbitrary code to run in response to an event, you must check the "Generate CallBack" box and enter the code in the code edit window.
- When you run the application once, all the required scripts will be generated and GNU Octave will be invoked.
- If you access the project folder, you will find the folders that store its parts.
