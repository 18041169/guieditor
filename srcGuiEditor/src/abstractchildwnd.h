/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ABSTRACTCHILDWND_H
#define ABSTRACTCHILDWND_H
#include <QCloseEvent>
#include <QMdiSubWindow>

class abstractChildWnd : public QMdiSubWindow
{
    Q_OBJECT
public:
    explicit abstractChildWnd(QWidget *parent, Qt::WindowFlags flags);
    virtual void save() = 0;
    virtual void save(QString) = 0;
    virtual void saveAs() = 0;
    virtual void load(QString fn) = 0;
    virtual void run(QString path) = 0;
    virtual void exportAsScript(QString path) = 0;

    bool documentHaveName;
    QString documentName;
signals:

public slots:

};

#endif // ABSTRACTCHILDWND_H
