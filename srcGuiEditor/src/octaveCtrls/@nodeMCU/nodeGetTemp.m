function retVal = nodeGetTemp(obj)
  v = [176, 11, 0, 0, 0, 0, 0, 0, 11];
  if (strcmp(class(obj), "nodeMCU") != 1)  
    error("Esta funcion ha sido definida para utilizar objetos de la clase nodeMCU");
  endif
 
 for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  for i=1:9
    tcp_write(obj.socket, uint8(v(i)));
  endfor
  b1 = tcp_read(obj.socket, 1);
#  disp(b1);
  b2 = tcp_read(obj.socket, 1);  
#  disp(b2);
  retVal = ((double(b1) + (double(b2) * 256.0)) * 625.0)/10000.0;
endfunction
