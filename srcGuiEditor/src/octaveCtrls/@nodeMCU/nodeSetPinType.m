function nodeSetPinType(obj, nroPin, tipoPin, varargin)
  v = [176, 0, nroPin, 0, 0, 0, 0, 0, 11];
  if (strcmp(class(obj), "nodeMCU") != 1)  
    error("Esta funcion ha sido definida para utilizar objetos de la clase nodeMCU");
  endif
  switch (tipoPin)
    case "digital" 
      v(2) = 1;
      val = varargin{1};
      if (strcmp(val, "outputOD"))
        v(4) = 2;
      elseif(strcmp(val, "output"))
        v(4) = 1;
      elseif(strcmp(val, "inputPullUp"))
        v(4) = 3;
      elseif(strcmp(val, "inputFloat") || strcmp(val, "input"))
        v(4) = 4;
      else
        error("Tipo de pin incorrecto");
      endif
      v(8) = 0;
      for i=1:7
        v(8) = v(8) + v(i);
      endfor
      v(8) = mod(v(8), 256);      
      for i=1:9
        tcp_write(obj.socket, uint8(v(i)));
      endfor      
    
    case "pwm"
      duty = -1;
      frec = -1;
      v(2) = 4;      
      for i=1:length(varargin)-1
        argName =  varargin{i};
        argVal =  varargin{i+1};
        if (strcmp(argName, 'duty'))
          duty = str2num(argVal);
        elseif(strcmp(argName, 'frec'))
          frec = str2num(argVal);	        
        endif
      endfor
      if ((frec == -1) || (duty == -1))
        error ("No se ha indicado frecuencia y ciclo de trabajo en configuracion pwm");
      else
        if (frec > 1000) frec = 1000; endif
        if (duty > 1023) duty = 1023; endif
        if (frec < 1)    frec = 1; endif 
        if (duty < 0)    duty = 0; endif 
        v(5) = mod(frec, 256);
        frec = idivide (frec, 256);
        v(4) = mod(frec, 256);
        
        v(7) = mod(duty, 256);
        duty = idivide (duty, 256);
        v(6) = mod(duty, 256);
        v(8) = 0;
        for i=1:7
          v(8) = v(8) + v(i);
        endfor
        v(8) = mod(v(8), 256);              
        for i=1:9
          tcp_write(obj.socket, uint8(v(i)));
        endfor        
      endif    
    case "adc"
      sourceADC = "";
      v(2) = 8;
      for i=1:length(varargin)-1
        argName =  varargin{i};
        argVal =  varargin{i+1};
        if (strcmp(argName, "source"))
          sourceADC = argVal;
        endif
      endfor
      if(strcmp(sourceADC, ""))
        error("Al configurar el pin adc debe indicar si se lo vincula al pin 0 o a vcc");
      endif
        if(strcmp(sourceADC, "input"))
          v(4) = 0;
        elseif (strcmp(sourceADC,"vcc"))
          v(4) = 1;
        else
          error("Error indicando origen de la entrada al adc");
        endif        
        v(8) = 0;
        for i=1:7
          v(8) = v(8) + v(i);
        endfor
        v(8) = mod(v(8), 256);              
        for i=1:9
          tcp_write(obj.socket, uint8(v(i)));
        endfor        
    otherwise
      error("Tipo de pin no soportado");
  endswitch
endfunction
