/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef ACTSIGNATURE_H
#define ACTSIGNATURE_H

#include "prgaction.h"
#include "scsignature.h"

class actSignature: public prgAction
{
protected:
    scSignature * cmd;
public:
    actSignature():prgAction()
    {
        cmd = new scSignature(this);
    }

    QString info()
    {
        return "Check device signtature (" + QString::number(cmd->getRcvData(), 16) + ")";
    }
    void getSerialCmd(QVector<serialCmd *> &toSend)
    {
        toSend.push_back(cmd);
    }
    ~actSignature(){
        delete cmd;
    }
};

#endif // ACTSIGNATURE_H
