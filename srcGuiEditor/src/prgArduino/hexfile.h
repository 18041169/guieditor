/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef HEXFILE_H
#define HEXFILE_H

#include <QString>
#include <QVector>
#include <stdint.h>

class hexRecord{
private:
    uint8_t *data;
    uint8_t  len;
    uint16_t addr;
    uint8_t  type;

public:
  hexRecord(int dataLen){
      len = dataLen;
      data = new uint8_t[len];
  }

  uint8_t  getLen(){
      return len;
  }

  uint16_t getAddr()
  {
      return addr;
  }
  void setAddr(uint16_t a)
  {
      addr = a;
  }

  uint8_t  getType()
  {
      return type;
  }

  void setType(uint8_t t)
  {
      type = t;
  }

  uint8_t  getData(int off)
  {
      return data[off];
  }

  uint8_t * getData()
  {
      return data;
  }

  void setData(int off, uint8_t v)
  {
      if ((off >= 0) && (off < len))
        data[off] = v;
  }

  uint8_t  getCrc(void)
  {
      uint8_t crc = this->len;
      crc +=  (addr & 0xFF);
      crc += ((addr >> 8) & 0xFF);
      crc +=  (type);
      for(int i = 0; i < len; i++)
          crc += data[i];
      crc = (~crc) + 1;
      return crc;
  }

  ~hexRecord()
  {
      delete []data;
  }

};


class memPage
{
private:
    uint32_t addr;
    uint8_t *data;
    uint32_t size;
public:
    memPage(uint32_t size, uint32_t addr);
    uint32_t  getAddr();
    uint8_t   getData(uint32_t off);
    uint8_t*  getData();
    void       setData(uint8_t val, uint32_t off);
    uint32_t  getSize();
    ~memPage();
};

class hexFile
{
  private:
    QString fn;
    QVector <hexRecord *> data;
    QVector <memPage *> pages;
  public:
    hexFile();
bool process(QString fileName);
void paginate(uint32_t pageSize);
uint32_t pageCount();
memPage * getPage(uint32_t p);
QVector<hexRecord *> hexData(void);
    ~hexFile();
};

#endif // HEXFILE_H
