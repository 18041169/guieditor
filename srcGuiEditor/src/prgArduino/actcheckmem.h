/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef ACTCHECKMEM_H
#define ACTCHECKMEM_H
#include "prgaction.h"
#include "sccheckmem.h"
#include "scloadpage.h"

class actCheckMem : public prgAction
{
protected:
    scCheckMem * cmdCheck;
    scLoadPage * cmdLoad;
    memPage *p;
public:
    actCheckMem(memPage *p):prgAction()
    {        
        cmdLoad = new scLoadPage(this, p);
        cmdCheck = new scCheckMem(this, p, true);
        this->p = p;
    }

    QString info()
    {
        QString ret;
        ret = "Reading addres: " + QString("0x%1").arg(p->getAddr(), 4, 16, QLatin1Char('0'));
        if(cmdCheck->checkOk())
            ret = ret + "[ok]";
        else
            ret = ret + "[fail]";
        return ret;
    }
    void getSerialCmd(QVector<serialCmd *> &toSend)
    {
        toSend.push_back(cmdLoad);
        toSend.push_back(cmdCheck);
    }
    ~actCheckMem(){
        delete cmdLoad;
        delete cmdCheck;
    }
};

#endif // ACTCHECKMEM_H
