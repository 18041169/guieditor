/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "scwritemem.h"
#include "actwritemem.h"

scWriteMem::scWriteMem(prgAction *act, memPage * page):serialCmd(act, true)
{
    this->buf.clear();
    this->aPage = page;
}

QString scWriteMem::cmdInfo()
{
    return "Writing page address: " + QString("0x%1").arg(aPage->getAddr(), 4, 16, QLatin1Char('0'));
}

QByteArray scWriteMem::dataToSend(void)
{
    QByteArray toSend;
    uint32_t i;
    toSend.push_back(Cmnd_STK_PROG_PAGE);
    toSend.push_back((uint8_t)((aPage->getSize()>> 8) & 0xFF));
    toSend.push_back((uint8_t)(aPage->getSize() & 0xFF));
    toSend.push_back((uint8_t)'F');
    for(i = 0; i < this->aPage->getSize(); i++)
        toSend.push_back((uint8_t)aPage->getData(i));
    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

rcvType scWriteMem::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    buf.push_back(data);
    if ((buf.length() == 2) && (buf[0] == Resp_STK_INSYNC) && (buf[1] == Resp_STK_OK))
        ret = rcvOk;
    else if((buf.length() == 1) && (buf[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

bool scWriteMem::mustSkip(void)
{
    return !(dynamic_cast<actWriteMem *>(associatedAction)->mustWrite());
}

scWriteMem::~scWriteMem(void)
{

}
