/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ACTSYNC_H
#define ACTSYNC_H
#include "prgaction.h"
#include "scsync.h"

class actSync: public prgAction
{
protected:
    scSync * cmd;
public:
    actSync():prgAction()
    {
        cmd = new scSync(this);
    }

    QString info()
    {
        return "Sync...";
    }
    void getSerialCmd(QVector<serialCmd *> &toSend)
    {
        toSend.push_back(cmd);
    }
    ~actSync(){
        delete cmd;
    }
};


#endif // ACTSYNC_H
