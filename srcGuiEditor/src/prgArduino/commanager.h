/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef COMMANAGER_H
#define COMMANAGER_H

#include <QObject>
#include <QSerialPort>
#include <QQueue>
#include <QTimer>
#include "hexfile.h"
#include "serialcmd.h"

enum comManagerState {stIdle, stRunning};
enum promptState {psNone, psOne, psTwo};

class comManager : public QObject
{
    Q_OBJECT

private:
    hexFile fw;
    unsigned int totalActCnt;
    unsigned int execActCnt;
    QString fwFileName;
    QSerialPort *serPort;
    QQueue<serialCmd *> cmdList;
    comManagerState state;
    QTimer timeOut;
    QQueue<prgAction *> actionList;
    void addCmd(serialCmd *);
    void addAction(prgAction *act);
    void executeCmd(void);
    void executeAction(void);
public:
    explicit comManager(QObject *parent = 0);
    virtual ~comManager(void);
    bool openDevice(QString port, QSerialPort::BaudRate baud);
    void closeDevice(void);
    bool isConnected(void);

    void start(QString FileName);

signals:
    void updateStateCmd(QString);
    void updateStateAct(QString);
    void updateStateAct(int, int);
    void programError(QString);

public slots:
    void dataReady(void);    
    void onTimeOut(void);
    void resetStart(void);
    void resetEnd(void);
};

#endif // COMMANAGER_H
