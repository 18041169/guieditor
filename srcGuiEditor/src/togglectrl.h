/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef TOGGLECTRL_H
#define TOGGLECTRL_H
#include <abstractuicctrl.h>
#include <QWidget>
#include <QPushButton>
#include <QList>
#include "controlgenerator.h"
#include <QDir>
#include <editabletoggle.h>
#include "wdgiconchooser.h"

class toggleCheckPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    toggleCheckPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void toggleCheckedChanged(QString text);
};



class textTogglePropEditor: public abstractPropEditor
{
public:
    textTogglePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QPushButton *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal){
        ((QPushButton *)this->ctrl->associatedWidget())->setText(newVal);
    }
    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};

class toggleBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QPalette palette;
public:
    toggleBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class toggleFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    toggleFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class toggleCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;
  QString m_iconFile;
public:
    static unsigned int getNameCounter();
    toggleCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    virtual QString className() { return "toggleCtrl";}
    virtual QStringList generateMFile(QString path);
    QString iconFile(void) {return this->m_iconFile;}

    void setIconFile(QString iFile){
        QPixmap p, q;
        if(this->getParentWnd()->getGUIPrj()){
          QString imgPath = this->getParentWnd()->getGUIPrj()->imgPath() + QDir::separator() + iFile ;
          if (QFile::exists(imgPath)){

            p = QPixmap(imgPath);
            ((editableToggle *)this->associatedWidget())->setIcon(QIcon(p));
          }
          else
            ((editableToggle *)this->associatedWidget())->setIcon(QIcon(QPixmap()));
          this->m_iconFile = iFile;
        }
    }
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~toggleCtrl();
};

class iconTogglePropEditor: public abstractPropEditor
{
     Q_OBJECT
public:
    iconTogglePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "icon";
    }

    QString getValue(){
        return ((toggleCtrl *)this->ctrl)->iconFile();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}

    virtual void setValue(QString newVal){
        if(newVal != "[None]")
          ((toggleCtrl *)this->ctrl)->setIconFile(newVal);
        else
          ((toggleCtrl *)this->ctrl)->setIconFile("");
    }
    virtual QWidget * getEditWidget(){
        QString iconFN;
        wdgIconChooser * wdgIconCh = new wdgIconChooser(this);
        iconFN = this->getValue();
        wdgIconCh->setIconFileName(iconFN);
        return wdgIconCh;
    }
    virtual QString generateCode()
    {
        QString fn = "";
        QString ret;
        QString prjImgVar = "[";
        if (((toggleCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()){
          fn = ((toggleCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()->imgPath() + QDir::separator() + this->getValue();
          prjImgVar= "[_" +  ((toggleCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()->name() + "ImgPath  filesep() ";
        }

        if (QFile::exists(fn) && (this->getValue() != "")){
            ret = "'cdata', _loadIcon(" + prjImgVar + "'" + this->getValue() + "'])";
        }
        else{
            ret = "'cdata', [ ]";
        }
        return ret;
    }
};
class toggleCtrlGen: public controlGenerator
{
public:
  toggleCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // TOGGLECTRL_H
