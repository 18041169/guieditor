function ret = _loadIcon (fn)
  ret = [];
  if exist(fn, "file") != 0
    [im, map, alpha ] = imread(fn);
    im=double(im)/255;
    for f=1:rows(alpha)
      for c=1:columns(alpha)
        if alpha(f,c) == 0
          im(f,c, 1) = NaN;
          im(f,c, 2) = NaN;
          im(f,c, 3) = NaN;
        endif
      endfor
    endfor
    ret = im;
  endif
endfunction
