/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WDGITEMEDITOR_H
#define WDGITEMEDITOR_H

#include <QWidget>
#include "abstractuicctrl.h"

namespace Ui {
class wdgItemEditor;
}

class wdgItemEditor : public QWidget, public updPropWdg
{
    Q_OBJECT

public:
    explicit wdgItemEditor(abstractPropEditor * prop);
    ~wdgItemEditor();
    void setItems(QString v);
    QString getItems();
    QLineEdit *getLEItems();
    virtual void updPropValue();

private slots:
    void on_btnEdit_clicked();

private:
    Ui::wdgItemEditor *ui;
};

#endif // WDGITEMEDITOR_H
