#include "painthidden.h"

paintHidden::paintHidden(QWidget *w)
{
    QPen pen;
    QPainter p;
    pen.setWidth(2);
    pen.setStyle(Qt::DashLine);
    pen.setColor(Qt::red);
    p.begin(w);
    p.setPen(pen);
    p.drawRect(w->rect().x()+1, w->rect().y()+1, w->rect().width() - 2, w->rect().height() - 2);
    p.end();
}

QString paintHidden::getTableHiddenStyle(void){
    QString ret = "QTableWidget[isHidden=true]{ border:2px dashed red;}";
    return ret;
}

QString paintHidden::getListHiddenStyle(void){
    QString ret = "QListWidget[isHidden=true]{ border:2px dashed red;}";
    return ret;
}
