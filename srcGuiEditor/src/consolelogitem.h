#ifndef CONSOLELOGITEM_H
#define CONSOLELOGITEM_H
#include <QListWidgetItem>
#include <QString>
#include "guiproject.h"

class ConsoleLogItem : public QListWidgetItem
{
private:
    guiProject * prj;
    QString m_file;
    QString m_function;
    int m_line;
    int m_col;
public:
    ConsoleLogItem(guiProject * prj, QListWidget *parent = nullptr);
    ConsoleLogItem(guiProject * prj, const QString &text, QListWidget *parent = nullptr);
    ConsoleLogItem(guiProject * prj,const QIcon &icon, const QString &text, QListWidget *parent = nullptr);
    guiProject * project(void){
        return this->prj;
    }
    void setFile(QString f){
        this->m_file = f;
    }
    QString file(void){
        return this->m_file;
    }
    void setFunction(QString f){
        this->m_function = f;
    }
    QString function(void){
        return this->m_function;
    }
    void setLine(int l){
        this->m_line = l;
    }
    int line(void){
        return this->m_line;
    }
    void setColumn(int c){
        this->m_col = c;
    }
    int column(void){
        return this->m_col;
    }
};

#endif // CONSOLELOGITEM_H
