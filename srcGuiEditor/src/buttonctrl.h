/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef BUTTONCTRL_H
#define BUTTONCTRL_H
#include <abstractuicctrl.h>
#include <QWidget>
#include <QPushButton>
#include <QList>
#include "controlgenerator.h"
#include <QDir>
#include <QFile>
#include "editablebutton.h"
#include "wdgiconchooser.h"

class textBtnPropEditor: public abstractPropEditor
{
public:
    textBtnPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QPushButton *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal){
        ((QPushButton *)this->ctrl->associatedWidget())->setText(newVal);
    }
    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};



class btnBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QPalette palette;
public:
    btnBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class btnFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    btnFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};


class buttonCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;
  QString m_iconFile;
public:
    static unsigned int getNameCounter();
    buttonCtrl(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    virtual QString className() { return "buttonCtrl";}
    QString iconFile(void) {return this->m_iconFile;}

    void setIconFile(QString iFile){
        QPixmap p, q;
        if(this->getParentWnd()->getGUIPrj()){
          QString imgPath = this->getParentWnd()->getGUIPrj()->imgPath() + QDir::separator() + iFile ;
          if (QFile::exists(imgPath)){

            p = QPixmap(imgPath);
            ((editableButton *)this->associatedWidget())->setIcon(QIcon(p));
          }
          else
            ((editableButton *)this->associatedWidget())->setIcon(QIcon(QPixmap()));
          this->m_iconFile = iFile;
        }
    }

    virtual QStringList generateMFile(QString path);
    QStringList createCallBack(QString path);
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~buttonCtrl();
};

class iconBtnPropEditor: public abstractPropEditor
{
     Q_OBJECT
public:
    iconBtnPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "icon";
    }
    virtual bool canGenerateCode(){
        return this->getValue() != "";
    }
    QString getValue(){
        return ((buttonCtrl *)this->ctrl)->iconFile();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}

    virtual void setValue(QString newVal){
        if(newVal != "[None]")
          ((buttonCtrl *)this->ctrl)->setIconFile(newVal);
        else
          ((buttonCtrl *)this->ctrl)->setIconFile("");
    }
    virtual QWidget * getEditWidget(){
        QString iconFN;
        wdgIconChooser * wdgIconCh = new wdgIconChooser(this);
        iconFN = this->getValue();
        wdgIconCh->setIconFileName(iconFN);
        return wdgIconCh;
    }
    virtual QString generateCode()
    {
        QString fn = "";
        QString ret;
        QString prjImgVar = "[";
        if (((buttonCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()){
          fn = ((buttonCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()->imgPath() + QDir::separator() + this->getValue();
          prjImgVar= "[_" +  ((buttonCtrl *)this->ctrl)->getParentWnd()->getGUIPrj()->name() + "ImgPath  filesep() ";
        }

        if (QFile::exists(fn) && (this->getValue() != "")){
            ret = "'cdata', _loadIcon(" + prjImgVar + "'" + this->getValue() + "'])";
        }
        else{
            /* This value never be used because the generation code property
             * change automacally to false when there is no image selected */
            ret = "'cdata', [ ]";
        }
        return ret;
    }
};

class buttonCtrlGen: public controlGenerator
{
public:
  buttonCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // BUTTONCTRL_H
