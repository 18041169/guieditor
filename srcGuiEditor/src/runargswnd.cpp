#include "runargswnd.h"
#include "ui_runargswnd.h"

runArgsWnd::runArgsWnd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::runArgsWnd)
{
    ui->setupUi(this);
}

QString runArgsWnd::getArguments(void){
    return ui->leArgs->text();
}

void runArgsWnd::setArguments(QString v){
    ui->leArgs->setText(v);
}

runArgsWnd::~runArgsWnd()
{
    delete ui;
}
