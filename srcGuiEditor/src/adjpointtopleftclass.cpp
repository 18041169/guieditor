/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "adjpointtopleftclass.h"
#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QDebug>
#include <QPaintEvent>

/*
 +
 +       > *------------*------------*
 +         |                         |
 +         *                         *
 +         |                         |
 +         *------------*------------*
 +
 */

adjPointTopLeftClass::adjPointTopLeftClass(QWidget *parent) :
    QWidget(parent)
{
    this->asociatedControl = NULL;
    this->canResize = true;
    this->resizing = false;

}
void adjPointTopLeftClass::asociateCtrl(QWidget * ctrl, bool canResize)
{
    this->canResize = canResize;
    if(this->canResize)
      this->setCursor(Qt::SizeFDiagCursor);

    this->asociatedControl = ctrl;
    this->setGeometry(this->asociatedControl->x()-3,
                      this->asociatedControl->y()-3,
                      5,
                      5);
}

void adjPointTopLeftClass::resetPosition()
{
    this->setGeometry(this->asociatedControl->x()-3,
                      this->asociatedControl->y()-3,
                      5,
                      5);
}

void adjPointTopLeftClass::paintEvent(QPaintEvent * event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    QBrush brush(QColor::fromRgb(120,120,120));
    QPen penBlack(Qt::black);
    QPen penLight(QColor::fromRgb(200,200,200));
    penBlack.setWidth(1);
    penBlack.setStyle(Qt::SolidLine);
    penLight.setWidth(1);
    penLight.setStyle(Qt::SolidLine);
    brush.setStyle(Qt::SolidPattern);
    painter.setPen(penLight);
    painter.setBrush(brush);
    this->setGeometry(this->asociatedControl->x()-2, this->asociatedControl->y()-2, 5, 5);
    painter.fillRect(0, 0, (int)this->width()-1, (int)this->height()-1,QColor::fromRgb(120,120,120));

    painter.drawLine(0, 0, (int)this->width()-1, 0);
    painter.drawLine(0, 0, 0, (int)this->height()-1);
    painter.setPen(penBlack);
    painter.drawLine((int)this->width()-1, (int)this->height()-1, 0, (int)this->height()-1);
    painter.drawLine((int)this->width()-1, (int)this->height()-1, (int)this->width()-1,0);
}


void adjPointTopLeftClass::mouseReleaseEvent(QMouseEvent * )
{
    if(canResize)
      this->resizing = false;
}

void adjPointTopLeftClass::mousePressEvent(QMouseEvent * event)
{
    if(canResize)
    {
      this->moveXInit = event->x();
      this->moveYInit = event->y();
      this->resizing = true;
    }
}

void adjPointTopLeftClass::mouseMoveEvent(QMouseEvent * event)
{
    if(this->resizing && this->canResize)
    {
        int dy = event->y()-this->moveYInit;
        int dx = event->x()-this->moveXInit;
        QRect g = this->asociatedControl->geometry();
        int origWidth = g.width();
        int origHeight = g.height();
        g.setLeft(g.left() + dx);
        g.setTop(g.top() + dy);
        g.setWidth(origWidth - dx);
        g.setHeight(origHeight - dy);
        this->asociatedControl->setGeometry(g);
    }
}

